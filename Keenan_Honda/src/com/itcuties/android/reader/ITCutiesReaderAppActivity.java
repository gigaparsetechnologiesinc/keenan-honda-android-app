package com.itcuties.android.reader;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.slide_menu.PostData;
import com.example.slide_menu.R;
import com.example.slide_menu.RssContent;

import com.itcuties.android.reader.data.RssItem;
import com.itcuties.android.reader.listeners.ListListener;
import com.itcuties.android.reader.util.RssReader;

/**
 * Main application activity.
 * 
 * @author ITCuties
 *
 */
public class ITCutiesReaderAppActivity extends Activity {
	/** 
	 * This method creates main application view
	 */

	RssReader rssReader;
	ListView itcItems;
	ArrayList prgmName;
	RssItem[] array;
    public static int [] prgmImages={R.drawable.ic_launcher,R.drawable.ic_launcher,R.drawable.ic_launcher,R.drawable.ic_launcher,R.drawable.ic_launcher,R.drawable.ic_launcher,R.drawable.ic_launcher,R.drawable.ic_launcher,R.drawable.ic_launcher};
    public static String [] prgmNameList={"Let Us C","c++","JAVA","Jsp","Microsoft .Net","Android","PHP","Jquery","JavaScript"};
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set view
		setContentView(R.layout.main);

	
		final String URL = getIntent().getStringExtra("URL");
		final String name = getIntent().getStringExtra("Name");
		TextView title = (TextView)findViewById(R.id.textView1);
		title.setText(name);
		

		runOnUiThread(new Runnable() {
		     @Override
		     public void run() {

		//stuff that updates ui
		    	//loadlistview l= new loadlistview(ITCutiesReaderAppActivity.this);
		 		//l.execute("");
		 		
		    	  	try {
		    	  		
		    			// Create RSS reader
		    			rssReader = new RssReader(URL);
		    			
		    	  		
		    	  		RssItem rsitem =  new RssItem();
		    			int a = rssReader.getItems().size();
		    			final int[] images = new int[a];
		    			final String[] titlet = new  String[a];
		    			
		    			Resources res = ITCutiesReaderAppActivity.this.getResources();
		    			XMLParser saxparser =  new XMLParser();

		    			array = rssReader.getItems().toArray(new RssItem[rssReader.getItems().size()]);
		    			
		    			
		    		
		    			
		    			for(int i=0;i<a;i++)
		    			{
		    				images[i]=R.drawable.honda;
		    				titlet[i] = array[i].toString();
		    			}
		    			itcItems = (ListView) findViewById(R.id.listView);
			        	itcItems.setAdapter(new CustomAdapter(ITCutiesReaderAppActivity.this, titlet,images));
		    			
		    			
		    			
		    			Toast.makeText(ITCutiesReaderAppActivity.this, array[0].toString()+"",0).show();
		    		
		    	
		    			
		    			//Toast.makeText(ITCutiesReaderAppActivity.this, a+"",0).show();		
		    			
		    			// Get a ListView from main view
		    			
		    			
		    			// Create a list adapter
		    			
		    			//ArrayAdapter<RssItem> adapter = new ArrayAdapter<RssItem>(this,android.R.layout.simple_list_item_1, rssReader.getItems());
		    			
		    			// Set list adapter for the ListView
		    			
		    			//itcItems.setAdapter(adapter);
		    			
		    			
		    			
		    			// Set list view item click listener
		    			
		    			
		    		} catch (Exception e) {
		    			Log.e("ITCRssReader", e.getMessage()+" Url Error");
		    		}
		        	
		        	
		        	
		        	
		        	
		        	
		    }
		});
		
		itcItems.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
			
			
			}
		});
	
		
		
				
	}
	
	private class loadlistview extends AsyncTask<String, Void, String> {

        private Context context;

        public loadlistview(Context current){
            this.context = current;
        }
        protected String doInBackground(String... params) {
        	
        	
      
        //	itcItems.setAdapter(new CustomAdapter(ITCutiesReaderAppActivity.this, titlet,images));
        	
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
        	
        }

        @Override
        protected void onPreExecute() {
        	try {
				//itcItems.setOnItemClickListener(new ListListener(rssReader.getItems(), ITCutiesReaderAppActivity.this));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }
	
}