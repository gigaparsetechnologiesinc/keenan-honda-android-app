package com.example.slide_menu;

import com.itcuties.android.reader.CustomAdapter;

import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class RoadSideRight extends Activity {

	ListView lst;
    public static int [] prgmImages={R.drawable.honda,R.drawable.honda};
    public static String [] prgmNameList={"866-864-5211"};
    
    public static int [] prgmImages1={R.drawable.honda,R.drawable.honda};
    public static String [] prgmNameList1={"American Honda Financial","Honda Lease Trust"};
    
    String source="";
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_road_side_right);
		
		lst = (ListView)findViewById(R.id.listview_roadside);
		
		Intent i = getIntent();
		source = i.getStringExtra("Source");
		
		runOnUiThread(new Runnable() {
		     @Override
		     public void run() {

		//stuff that updates ui
		    	//loadlistview l= new loadlistview(ITCutiesReaderAppActivity.this);
		 		//l.execute("");
		 		
		    	  	try {
		    	  		
		    			// Create RSS reader
		    			int a = 5;
		    				
		    			if(source.equals("9"))
		    			{
		    				lst.setAdapter(new CustomAdapter(RoadSideRight.this, prgmNameList1,prgmImages1));
		    			}
		    			else
		    			{
		    				lst.setAdapter(new CustomAdapter(RoadSideRight.this, prgmNameList,prgmImages));
		    			}
		    			//Toast.makeText(ITCutiesReaderAppActivity.this, array[0].toString()+"",0).show();
		    		
		    	
		    			
		    			//Toast.makeText(ITCutiesReaderAppActivity.this, a+"",0).show();		
		    			
		    			// Get a ListView from main view
		    			
		    			
		    			// Create a list adapter
		    			
		    			//ArrayAdapter<RssItem> adapter = new ArrayAdapter<RssItem>(this,android.R.layout.simple_list_item_1, rssReader.getItems());
		    			
		    			// Set list adapter for the ListView
		    			
		    			//itcItems.setAdapter(adapter);
		    			
		    			
		    			
		    			// Set list view item click listener
		    			
		    			
		    		} catch (Exception e) {
		    			Log.e("ITCRssReader", e.getMessage()+" Url Error");
		    		}
		        	
		        	
		        	
		        	
		        	
		        	
		    }
		});
		
		lst.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				if(source.equals("1"))
				{
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					callIntent.setData(Uri.parse("tel:+1866-864-5211"));
					startActivity(callIntent);
				}
				else
				{
					if(arg2==0)
					{
						Intent i = new Intent(RoadSideRight.this, AmericalHonda.class);
						startActivity(i);
						
					}
					else
					{
						Intent i = new Intent(RoadSideRight.this, HondaLease.class);
						startActivity(i);						
					}
				}
			}
		});
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.road_side_right, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
