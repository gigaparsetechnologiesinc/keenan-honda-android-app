package com.example.slide_menu;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ourwebsite extends Activity{
	WebView w;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ourwebsite);
	
		
		w = (WebView)findViewById(R.id.ourwebsite_web);
		w.getSettings().setJavaScriptEnabled(true);
		w.setWebViewClient(new WebViewClient());
		w.getSettings().setLightTouchEnabled(true);
		w.getSettings().setLoadsImagesAutomatically(true);
		w.loadUrl("http://www.keenanhonda.com");
		
	}
	
}
