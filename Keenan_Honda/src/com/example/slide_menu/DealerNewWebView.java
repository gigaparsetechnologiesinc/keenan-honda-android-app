package com.example.slide_menu;

import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

public class DealerNewWebView extends Activity {

	WebView w;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dealer_new_web_view);
		
		w = (WebView)findViewById(R.id.d_webview);
		w.getSettings().setJavaScriptEnabled(true);
		w.loadUrl("file:///android_asset/www/index.html");
		
		
		
		
	}

	
}
