package com.example.slide_menu;

import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class DigitalGlobeContent extends Activity {

	WebView w;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_digital_globe_content);
		
		Intent i =getIntent();
		String url = i.getStringExtra("DigitalUrl");
		int ind = i.getIntExtra("index", 4);
		
		//Toast.makeText(DigitalGlobeContent.this, ""+ind, 0).show();
		
		/*w = (WebView)findViewById(R.id.digital_web);
		w.getSettings().setJavaScriptEnabled(true);
		w.setWebViewClient(new WebViewClient());
		w.getSettings().setLightTouchEnabled(true);
		w.getSettings().setLoadsImagesAutomatically(true);
		
		if(ind==4)
		{
			w.getSettings().setBuiltInZoomControls(false);
			w.getSettings().setUseWideViewPort(true);
			w.getSettings().setJavaScriptEnabled(true);
			w.getSettings().setSupportMultipleWindows(true);
			w.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
			w.getSettings().setLoadsImagesAutomatically(true);
			w.getSettings().setLightTouchEnabled(true);
			w.getSettings().setDomStorageEnabled(true);
			w.getSettings().setLoadWithOverviewMode(true);
			w.getSettings().setLoadsImagesAutomatically(true);
			w.setScrollbarFadingEnabled(false);
			w.loadUrl(url);
		}
		else
		{	
			w.loadUrl(url);
		}
		*/
		w = (WebView)findViewById(R.id.digital_web);
		w.getSettings().setJavaScriptEnabled(true);
		w.setWebViewClient(new WebViewClient());
		w.getSettings().setLightTouchEnabled(true);
		w.getSettings().setLoadsImagesAutomatically(true);
		w.setScrollbarFadingEnabled(false);
		
		w.getSettings().setBuiltInZoomControls(false);
		w.getSettings().setUseWideViewPort(true);
		w.getSettings().setJavaScriptEnabled(true);
		w.getSettings().setSupportMultipleWindows(true);
		w.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		w.getSettings().setLoadsImagesAutomatically(true);
		w.getSettings().setLightTouchEnabled(true);
		w.getSettings().setDomStorageEnabled(true);
		w.getSettings().setLoadWithOverviewMode(true);
		
		w.loadUrl(url);
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.digital_globe_content, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
