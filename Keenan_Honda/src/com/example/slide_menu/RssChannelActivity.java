package com.example.slide_menu;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.slide_menu.data.*;
import com.example.listeners.*;
import com.example.slide_menu.*;
import com.example.util.RssReader;

/**
 * Each category tab activity.
 * @author itcuties
 *
 */
public class RssChannelActivity extends Activity {
	
	// A reference to this activity
    private RssChannelActivity local;
    ProgressDialog pd;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_rss_channel);
		
		// Get the RSS URL that was set in the RssTabActivity
		//String rssUrl = (String)getIntent().getExtras().get("rss-url");
		
		// Set reference to this activity
        local = this;
         
        GetRSSDataTask task = new GetRSSDataTask();
         
        // Start process RSS task
        task.execute("http://www.keenanhonda.com/blog/feed/");
		
	}
	
	/**
	 * This class downloads and parses RSS Channel feed.
	 * 
	 * @author itcuties
	 *
	 */
	private class GetRSSDataTask extends AsyncTask<String, Void, List<RssItem> > {
        @Override
        protected List<RssItem> doInBackground(String... urls) {
            try {
                // Create RSS reader
                RssReader rssReader = new RssReader(urls[0]);
             
                // Parse RSS, get items
                return rssReader.getItems();
             
            } catch (Exception e) {
                Log.e("RssChannelActivity", e.getMessage());
            }
             
            
            
            return null;
        }
         
        @Override
        protected void onPreExecute() {
        	// TODO Auto-generated method stub
        	super.onPreExecute();
            pd = new ProgressDialog(RssChannelActivity.this);
            pd.setMessage("loading");
            pd.show();
        }
        

        
        
		@Override
        protected void onPostExecute(List<RssItem> result) {
           
		
	
           int a = result.size(); 
		   String[] itemname = new String[a];	
		   Integer[] img = new Integer[a];
           for(int i=0; i<a;i++)
           {
        	   itemname[i]=result.get(i).getTitle();
        	   img[i] = R.drawable.ic_drawer;
        	   
           }
           
           Toast.makeText(RssChannelActivity.this, a+"", 0).show();
            
            // Get a ListView from the RSS Channel view
            ListView itcItems = (ListView) findViewById(R.id.rssChannelListView);
               
            CustomListAdapter adapter=new CustomListAdapter(RssChannelActivity.this, itemname, img);
    		itcItems.setAdapter(adapter);
    		
            pd.dismiss();
            // Create a list adapter
            
           // ArrayAdapter<RssItem> adapter = new ArrayAdapter<RssItem>(local,R.layout.activity_rss_channel_item, result);
            // Set list adapter for the ListView
           
            //itcItems.setAdapter(adapter);
                         
            // Set list view item click listener
            itcItems.setOnItemClickListener(new ListListener(result, local));
        }
    }
	
}
