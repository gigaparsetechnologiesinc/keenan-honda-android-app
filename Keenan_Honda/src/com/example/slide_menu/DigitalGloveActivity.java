package com.example.slide_menu;


import java.util.ArrayList;

import com.itcuties.android.reader.CustomAdapter;
import com.itcuties.android.reader.ITCutiesReaderAppActivity;
import com.itcuties.android.reader.XMLParser;
import com.itcuties.android.reader.data.RssItem;
import com.itcuties.android.reader.listeners.ListListener;
import com.itcuties.android.reader.util.RssReader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DigitalGloveActivity extends Activity {


	RssReader rssReader;
	ListView itcItems;
	ArrayList prgmName;
    public static int [] prgmImages={R.drawable.honda,R.drawable.honda,R.drawable.honda,R.drawable.honda,R.drawable.honda};
    public static String [] prgmNameList={"Like us on Facebook!","Follow us on Twitter!",
    	"YouTube","Google+","Blog"};
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set view
		setContentView(R.layout.main);

	
		final String URL = getIntent().getStringExtra("URL");
		final String name = getIntent().getStringExtra("Name");
		TextView title = (TextView)findViewById(R.id.textView1);
		title.setText(name);
		

		runOnUiThread(new Runnable() {
		     @Override
		     public void run() {

		//stuff that updates ui
		    	//loadlistview l= new loadlistview(ITCutiesReaderAppActivity.this);
		 		//l.execute("");
		 		
		    	  	try {
		    	  		
		    			// Create RSS reader
		    			int a = 5;
		    				
		    		
		    			itcItems = (ListView) findViewById(R.id.listView);
			        	itcItems.setAdapter(new CustomAdapter(DigitalGloveActivity.this, prgmNameList,prgmImages));
		    			
		    			//Toast.makeText(ITCutiesReaderAppActivity.this, array[0].toString()+"",0).show();
		    		
		    	
		    			
		    			//Toast.makeText(ITCutiesReaderAppActivity.this, a+"",0).show();		
		    			
		    			// Get a ListView from main view
		    			
		    			
		    			// Create a list adapter
		    			
		    			//ArrayAdapter<RssItem> adapter = new ArrayAdapter<RssItem>(this,android.R.layout.simple_list_item_1, rssReader.getItems());
		    			
		    			// Set list adapter for the ListView
		    			
		    			//itcItems.setAdapter(adapter);
		    			
		    			
		    			
		    			// Set list view item click listener
		    			
		    			
		    		} catch (Exception e) {
		    			Log.e("ITCRssReader", e.getMessage()+" Url Error");
		    		}
		        	
		        	
		        	
		        	
		        	
		        	
		    }
		});
		
		itcItems.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
			
				//Toast.makeText(DigitalGloveActivity.this, "Index"+arg2, 0).show();
				String url = "";
				if(arg2==0)
				{
					url ="https://www.facebook.com/keenanhonda";
				}
				else if(arg2==1)
				{
					url="https://twitter.com/keenanhonda_pa";
				}
				else if(arg2==2)
				{
					url="https://www.youtube.com/user/keenanhondapa";
				}
				else if(arg2==3)
				{
					url="https://plus.google.com/117549388789999762317/posts?hl=en";
				}
				else if(arg2==4)
				{
					url="http://www.keenanhonda.com/blog";
				}
				Intent i  = new Intent(DigitalGloveActivity.this, DigitalGlobeContent.class);
				i.putExtra("DigitalUrl",url);
				i.putExtra("index",arg2);
				startActivity(i);
				
			}
		});
		
	}
	
	private class loadlistview extends AsyncTask<String, Void, String> {

        private Context context;

        public loadlistview(Context current){
            this.context = current;
        }
        protected String doInBackground(String... params) {
        	
        	
      
        //	itcItems.setAdapter(new CustomAdapter(ITCutiesReaderAppActivity.this, titlet,images));
        	
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
        	
        }

        @Override
        protected void onPreExecute() {
        	try {
				//itcItems.setOnItemClickListener(new ListListener(rssReader.getItems(), DigitalGloveActivity.this));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }
}
