package com.example.slide_menu;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;




import com.itcuties.android.reader.DOM_Parser;
import com.itcuties.android.reader.ITCutiesReaderAppActivity;
import com.itcuties.android.reader.XMLParser;
import com.itcuties.android.reader.data.RssItem;
import com.itcuties.android.reader.listeners.ListListener;
import com.itcuties.android.reader.util.RssReader;
import com.navdrawer.SimpleSideDrawer;
import com.td.rssreader.*;
import com.td.rssreader.parser.RSSFeed;

public class MainActivity extends Activity {
	SimpleSideDrawer slide_me;
	ImageView left_button, right_button;
	
	private TableLayout rlBg;
	
	ImageView menu1, menu2, menu3, menu4, menu5, menu6, menu7, menu8, menu9;
	
	RelativeLayout dealernews;
	
	
	RssReader rssReader;
	ListView itcItems;
	ArrayList prgmName;

	private String RSSFEEDURL = "http://www.mobilenations.com/rss/mb.xml";
	RSSFeed feed;
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int height = displaymetrics.heightPixels;
		int width = displaymetrics.widthPixels;
		Toast.makeText(MainActivity.this, width+" "+height, 0).show();
		if(width==240 && height==320)
		{
			setContentView(R.layout.activity_main_menu_240_320);
		}
		else if(width==240 && height==400)
		{
			setContentView(R.layout.activity_main_menu_240_400);
		}
		else if(width==240 && height==432)
		{
			setContentView(R.layout.activity_main_menu_240_432);
		}
		else if(width==280 && height==280)
		{
			setContentView(R.layout.activity_main_menu_280_280);
		}
		else if(width==320 && height==320)
		{
			setContentView(R.layout.activity_main_menu_320_320);
		}
		else if(width==320 && height==480)
		{
			setContentView(R.layout.activity_main_menu_320_480);
		}		
		else if(width==480 && height==800)
		{
			setContentView(R.layout.activity_main_menu_480_800);
		}		
		else if(width==480 && height==854)
		{
			setContentView(R.layout.activity_main_menu_480_854);
		}		
		else if(width==720 && height==1280)
		{
			setContentView(R.layout.activity_main_menu_720_1280);
		}		
		else if(width==768 && height==1280)
		{
			setContentView(R.layout.activity_main_menu_768_1280);
		}		
		else if(width==800 && height==1280)
		{
			setContentView(R.layout.activity_main_menu_800_1280);
		}		
		else if(width==1024 && height==600)
		{
			setContentView(R.layout.activity_main_menu_1024_600);
		}		
		else if(width==1080 && height==1920)
		{
			setContentView(R.layout.activity_main_menu_1080_1920);
		}		
		else if(width==1200 && height==1920)
		{
			setContentView(R.layout.activity_main_menu_1200_1920);
		}		
		else if(width==1280 && height==720)
		{
			setContentView(R.layout.activity_main_menu_1280_720);
		}		
		else if(width==1920 && height==1080)
		{
			setContentView(R.layout.activity_main_menu_280_280);
		}		
		else if(width==2560 && height==1600)
		{
			setContentView(R.layout.activity_main_menu_280_280);
		}	
		
		
		
		 
		//setContentView(R.layout.activity_main);
		
		menu1 = (ImageView)findViewById(R.id.menu1);
		menu2 = (ImageView)findViewById(R.id.menu2);
		menu3 = (ImageView)findViewById(R.id.menu3);
		menu4 = (ImageView)findViewById(R.id.menu4);
		menu5 = (ImageView)findViewById(R.id.menu5);
		menu6 = (ImageView)findViewById(R.id.menu6);
		menu7 = (ImageView)findViewById(R.id.menu7);
		menu8 = (ImageView)findViewById(R.id.menu8);
		menu9 = (ImageView)findViewById(R.id.menu9);
		
		
		menu1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent i = new Intent(MainActivity.this, RoadSideRight.class);
				i.putExtra("Source", "1");
				startActivity(i);
			}
		});
		
		menu2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this, eScheduling.class);
				startActivity(i);				
			}
		});
		
		menu3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// click for dealer news
				
				Intent i = new Intent(MainActivity.this,ITCutiesReaderAppActivity.class);
				i.putExtra("URL","http://www.keenanhonda.com/blog/feed/");
				i.putExtra("Name","Dealernews");
				startActivity(i);
				
			}
		});
		
		
		menu4.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Honda owner link
				
				Intent i = new Intent(MainActivity.this, hondaownerlink.class);
				startActivity(i);	
			}
		});
		
		menu5.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			    // Rewards	
			}
		});
		
		menu6.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			    // Specials
				
				Intent i = new Intent(MainActivity.this, Special.class);
				startActivity(i);				
			}
		});
		
		menu7.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			    // showroom	
				
				Intent i = new Intent(MainActivity.this, Showroom.class);
				startActivity(i);						
			}
		});
		
		menu8.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			    
				Intent i = new Intent(MainActivity.this, localgasprice.class);
				startActivity(i);			
			}
		});
		
		menu9.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this, RoadSideRight.class);
				i.putExtra("Source", "9");
				startActivity(i);	
			}
		});
		
		slide_me = new SimpleSideDrawer(this);
		slide_me.setLeftBehindContentView(R.layout.left_menu);
		slide_me.setRightBehindContentView(R.layout.right_menu);

		left_button = (ImageView) findViewById(R.id.leftMenu);
		right_button = (ImageView) findViewById(R.id.rightMenu);
		left_button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				slide_me.toggleLeftDrawer();
			}
		});
		right_button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				slide_me.toggleRightDrawer();
			}
		});
		
		ImageView toolkit1 = (ImageView)slide_me.findViewById(R.id.toolkitimage);
		TextView  toolkit2 = (TextView)slide_me.findViewById(R.id.toolkittext);

		ImageView dealernews1 = (ImageView)slide_me.findViewById(R.id.dealernewsimage);
		TextView  dealernews2 = (TextView)slide_me.findViewById(R.id.dealernewstext);
		
		ImageView manufac1 = (ImageView)slide_me.findViewById(R.id.manufacturenewsimage);
		TextView  manufac2 = (TextView)findViewById(R.id.manufacturenewstext);
		
		ImageView digital1 = (ImageView)slide_me.findViewById(R.id.digitalgloveboximage);
		TextView  digital2 = (TextView)slide_me.findViewById(R.id.digitalgloveboxtext);
		
		ImageView aboutus1 = (ImageView)slide_me.findViewById(R.id.aboutusimage);
		TextView  aboutus2 = (TextView)slide_me.findViewById(R.id.aboutustext);
		
		/*ImageView setting1 = (ImageView)slide_me.findViewById(R.id.settingimage);
		TextView  setting2 = (TextView)slide_me.findViewById(R.id.settingtext);
		*/
		
		ImageView callmynumber  =  (ImageView)slide_me.findViewById(R.id.leftmenu22);
		TextView  callmynumber1 =  (TextView)slide_me.findViewById(R.id.firstleft);
		
		ImageView visitwebsite = (ImageView)slide_me.findViewById(R.id.leftmenu23);
		TextView  visitwebsite1 = (TextView)slide_me.findViewById(R.id.second);
		
		ImageView getDirection = (ImageView)slide_me.findViewById(R.id.leftmenu24);
		TextView  getDirection1 = (TextView)slide_me.findViewById(R.id.thrird);		
		
		ImageView roadside = (ImageView)slide_me.findViewById(R.id.leftmenu25);
		TextView  roadside1 = (TextView)slide_me.findViewById(R.id.four);		
		
		ImageView feedback = (ImageView)slide_me.findViewById(R.id.leftmenu26);
		TextView  feedback1 = (TextView)slide_me.findViewById(R.id.five);			
		
		toolkit1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Toast.makeText(MainActivity.this, "Toolkit", 0).show();
				slide_me.toggleLeftDrawer();	
			}
		});
		
		toolkit2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Toast.makeText(MainActivity.this, "Toolkit", 0).show();
				slide_me.toggleLeftDrawer();
			}
		});
		
		dealernews1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				/*Intent i = new Intent(MainActivity.this,ITCutiesReaderAppActivity.class);
				i.putExtra("URL","http://www.keenanhonda.com/blog/feed/");
				i.putExtra("Name","Dealernews");
				startActivity(i);
				*/
				
				Intent i = new Intent(MainActivity.this,DealerNewWebView.class);
				startActivity(i);
				
				
				
				/*DOM_Parser myParser = new DOM_Parser();
				feed = myParser.parseXml(RSSFEEDURL);
				
				Bundle bundle = new Bundle();
				bundle.putSerializable("feed", feed);
				 
				// launch List activity
				Intent intent = new Intent(MainActivity.this, ListActivity.class);
				intent.putExtras(bundle);
				startActivity(intent);
				*/
				
				//loadlistview l= new loadlistview(MainActivity.this);
				slide_me.toggleLeftDrawer();
				
			}
		});
		
		dealernews2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				/*Intent i = new Intent(MainActivity.this,ITCutiesReaderAppActivity.class);
				i.putExtra("URL", "http://www.keenanhonda.com/blog/feed/");
				i.putExtra("Name","Dealer News");
				startActivity(i);
				
				*/

				Intent i = new Intent(MainActivity.this,DealerNewWebView.class);
				startActivity(i);				
				
				
				
				/*DOM_Parser myParser = new DOM_Parser();
				feed = myParser.parseXml(RSSFEEDURL);
				
				Bundle bundle = new Bundle();
				bundle.putSerializable("feed", feed);
				 
				// launch List activity
				Intent intent = new Intent(MainActivity.this, ListActivity.class);
				intent.putExtras(bundle);
				startActivity(intent);
				*/
				
				
				
				//loadlistview l= new loadlistview(MainActivity.this);
				slide_me.toggleLeftDrawer();
			}
		});
		
		manufac1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				Intent i = new Intent(MainActivity.this,ITCutiesReaderAppActivity.class);
				i.putExtra("URL", "http://feeds.feedburner.com/honda/news-honda-automobiles");
				i.putExtra("Name","Manufacture News");
				startActivity(i);
				//loadlistview l= new loadlistview(MainActivity.this);
				slide_me.toggleLeftDrawer();
			}
		});
		
		manufac2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this,ITCutiesReaderAppActivity.class);
				i.putExtra("URL", "http://feeds.feedburner.com/honda/news-honda-automobiles");
				i.putExtra("Name","Manufacture News");
				startActivity(i);
				//loadlistview l= new loadlistview(MainActivity.this);
				slide_me.toggleLeftDrawer();
			}
		});
		
		digital1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i =new Intent();
				i.setClass(getApplicationContext(), DigitalGloveActivity.class);
				startActivity(i);
				
				/*Intent i = new Intent(MainActivity.this,ITCutiesReaderAppActivity.class);
				i.putExtra("URL", "http://feeds.feedburner.com/honda/news-honda-automobiles");
				i.putExtra("Name","Digital Glove Box");
				startActivity(i);*/
				//loadlistview l= new loadlistview(MainActivity.this);
				slide_me.toggleLeftDrawer();
			}
		});
		
		digital2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent i =new Intent();
				i.setClass(getApplicationContext(), DigitalGloveActivity.class);
				startActivity(i);
				
				
				// TODO Auto-generated method stub
				/*Intent i = new Intent(MainActivity.this,ITCutiesReaderAppActivity.class);
				i.putExtra("URL", "http://feeds.feedburner.com/honda/news-honda-automobiles");
				i.putExtra("Name","Digital Glove Box");
				startActivity(i);*/
				//loadlistview l= new loadlistview(MainActivity.this);
				slide_me.toggleLeftDrawer();
			}
		});
		
		aboutus1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this,aboutus.class);
				//i.putExtra("URL", "http://feeds.feedburner.com/honda/news-honda-automobiles");
				//i.putExtra("Name","Digital Glove Box");
				startActivity(i);
				//loadlistview l= new loadlistview(MainActivity.this);
				slide_me.toggleLeftDrawer();
			}
		});
		
		aboutus2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this,aboutus.class);
				//i.putExtra("URL", "http://feeds.feedburner.com/honda/news-honda-automobiles");
				//i.putExtra("Name","Digital Glove Box");
				startActivity(i);
				//loadlistview l= new loadlistview(MainActivity.this);
				slide_me.toggleLeftDrawer();
			}
		});
		
	/*	setting1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(MainActivity.this, "Setting", 0).show();
			}
		});
		
		setting2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(MainActivity.this, "Setting", 0).show();
			}
		});*/
		
		callmynumber.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse("tel:+1877-621-4216"));
				startActivity(callIntent);

			}
		});
		
		callmynumber1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse("tel:+1877-621-4216"));
				startActivity(callIntent);			
				
			}
		});
		
	
		visitwebsite.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this,ourwebsite.class);
				startActivity(i);
				//loadlistview l= new loadlistview(MainActivity.this);
				slide_me.toggleLeftDrawer();				
			}
		});
		
		visitwebsite1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this,ourwebsite.class);
				startActivity(i);
				//loadlistview l= new loadlistview(MainActivity.this);
				slide_me.toggleLeftDrawer();					
			}
		});
		
		
		roadside.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this, RoadSideRight.class);
				i.putExtra("Source", "1");
				startActivity(i);
				//loadlistview l= new loadlistview(MainActivity.this);
				slide_me.toggleLeftDrawer();			
				
			}
		});
		
		roadside1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this, RoadSideRight.class);
				i.putExtra("Source", "1");
				startActivity(i);
				//loadlistview l= new loadlistview(MainActivity.this);
				slide_me.toggleLeftDrawer();			
				
			}
		});		
		
		feedback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i  = new Intent(MainActivity.this, DigitalGlobeContent.class);
				i.putExtra("DigitalUrl","http://www.keenanhonda.com/keenan-honda-dealership-customer-reviews");
				i.putExtra("index",0);
				startActivity(i);	
			}
		});
		
	 feedback1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i  = new Intent(MainActivity.this, DigitalGlobeContent.class);
				i.putExtra("DigitalUrl","http://www.keenanhonda.com/keenan-honda-dealership-customer-reviews");
				i.putExtra("index",0);
				startActivity(i);	
			}
		});		
		
	}
	
	
	
	
	
	
	private class LoadBackground extends AsyncTask<String, Void, Drawable> {

	    private String imageUrl , imageName;

	    public LoadBackground(String url, String file_name) {
	        this.imageUrl = url;
	        this.imageName = file_name;
	    }

	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	    }

	    @Override
	    protected Drawable doInBackground(String... urls) {

	        try {
	            InputStream is = (InputStream) this.fetch(this.imageUrl);
	            Drawable d = Drawable.createFromStream(is, this.imageName);
	            return d;
	        } catch (MalformedURLException e) {
	            e.printStackTrace();
	            return null;
	        } catch (IOException e) {
	            e.printStackTrace();
	            return null;
	        }
	    }
	    private Object fetch(String address) throws MalformedURLException,IOException {
	        URL url = new URL(address);
	        Object content = url.getContent();
	        return content;
	    }

	    @SuppressWarnings("deprecation")
	    @Override
	    protected void onPostExecute(Drawable result) {
	        super.onPostExecute(result);
	        rlBg.setBackgroundDrawable(result);
	    }
	   
	}
	public class leftmenu extends Activity
	{
		@Override
		protected void onCreate(Bundle savedInstanceState) {
		    super.onCreate(savedInstanceState);
		   setContentView(R.layout.left_menu);
		   
		   	
		   
		}
	}
	
	
	
		
	
}
