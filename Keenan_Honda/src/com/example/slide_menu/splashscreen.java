package com.example.slide_menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;


public class splashscreen extends Activity {

	   private final int WAIT_TIME = 2500;
	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		System.out.println("LoadingScreenActivity  screen started");
		setContentView(R.layout.loading_screen);
		findViewById(R.id.mainSpinner1).setVisibility(View.VISIBLE);

		new Handler().postDelayed(new Runnable(){ 
		@Override 
		    public void run() { 
	               //Simulating a long running task
			     try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		          System.out.println("Going to Profile Data");
		         /* Create an Intent that will start the ProfileData-Activity. */
	              Intent mainIntent = new Intent(splashscreen.this,MainActivity.class); 
	              splashscreen.this.startActivity(mainIntent); 
	              splashscreen.this.finish(); 
		} 
		}, WAIT_TIME);
	      }
}
