package com.example.slide_menu;

import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class AmericalHonda extends Activity {

	WebView w;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_americal_honda);
		
		w = (WebView)findViewById(R.id.americalH);
		w.getSettings().setJavaScriptEnabled(true);
		w.setWebViewClient(new WebViewClient());
		w.getSettings().setLightTouchEnabled(true);
		w.getSettings().setLoadsImagesAutomatically(true);
		w.setScrollbarFadingEnabled(false);
		
		w.getSettings().setBuiltInZoomControls(false);
		w.getSettings().setUseWideViewPort(true);
		w.getSettings().setJavaScriptEnabled(true);
		w.getSettings().setSupportMultipleWindows(true);
		w.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		w.getSettings().setLoadsImagesAutomatically(true);
		w.getSettings().setLightTouchEnabled(true);
		w.getSettings().setDomStorageEnabled(true);
		w.getSettings().setLoadWithOverviewMode(true);
		w.loadUrl("http://www.hondafinancialservices.com");
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.americal_honda, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
