package com.example.slide_menu;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

public class localgasprice extends Activity{
	WebView w;
	ImageView back;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.local_gas_price);

		back = (ImageView)findViewById(R.id.back2localgas);
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				finish();
			}
		});
		w = (WebView)findViewById(R.id.local_gas);
		w.getSettings().setJavaScriptEnabled(true);
		w.setWebViewClient(new WebViewClient());
		w.getSettings().setLightTouchEnabled(true);
		w.getSettings().setLoadsImagesAutomatically(true);
		w.loadUrl("https://m.gasbuddy.com/?gbTouch");		
		
	}
}
