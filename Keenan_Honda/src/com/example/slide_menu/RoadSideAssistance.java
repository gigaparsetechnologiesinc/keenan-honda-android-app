package com.example.slide_menu;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

public class RoadSideAssistance extends Activity{

	WebView w;
	ImageView back;
	private File cacheDir;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.roadsideassistance);
	

		
        w = (WebView)findViewById(R.id.roadside_web);
		w.getSettings().setJavaScriptEnabled(true);
		w.setWebViewClient(new WebViewClient());
		w.loadUrl("http://www.keenanhonda.com/used-cars-doylestown-pa");
		
		
		/*back = (ImageView)findViewById(R.id.backimage);
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				finish();
				
			}
		});
		*/

		
		
	}
	
	
	public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                    
                }
            }
        }

        return dir.delete();
    }
}
